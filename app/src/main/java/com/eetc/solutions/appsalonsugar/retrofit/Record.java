package com.eetc.solutions.appsalonsugar.retrofit;

/**
 * Created by alex on 18.09.2016.
 */
public class Record {
    private int id_service;
    private int id_master;
    private String date;
    private String time;

    public int getId_service() {
        return id_service;
    }

    public void setId_service(int id_service) {
        this.id_service = id_service;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public  void setDate(String date) {
        this.date = date;
    }

    public int getId_master() {
        return id_master;
    }

    public void setId_master(int id_master) {
        this.id_master = id_master;
    }
}
