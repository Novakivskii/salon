package com.eetc.solutions.appsalonsugar.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.adapter.ExpListViewAdapterWithCheckbox;
import com.eetc.solutions.appsalonsugar.retrofit.Constants;
import com.eetc.solutions.appsalonsugar.retrofit.response.JSONResponse;
import com.eetc.solutions.appsalonsugar.retrofit.request.RequestInterface;
import com.eetc.solutions.appsalonsugar.retrofit.Services;
import com.eetc.solutions.appsalonsugar.util.FragmentUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SelectServices extends Fragment {
    View view;
     ExpListViewAdapterWithCheckbox listAdapter;
    ExpandableListView expListView;
    ArrayList<String> listDataHeader;
    HashMap<String, List<Services>> listDataChild;
    ArrayList<Services> haircut;
    ArrayList<Services> cosmetology;
    ArrayList<Services> manicure;
    ArrayList<Services> massage;
 Button button_next;
    SelectMaster selectMaster = new SelectMaster();
     List<Services> serviceList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_service, container, false);
        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);
        button_next = (Button) view.findViewById(R.id.button_next);
        prepareListData();
        listDataChild = new HashMap<String, List<Services>>();
        serviceList = new ArrayList<>();
        LoadServiceAsync loadServiceAsync = new LoadServiceAsync();
        loadServiceAsync.execute();


        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getContext(),
                        listDataChild.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                    int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(
                        getContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get((groupPosition)).get(childPosition).getTitle(), Toast.LENGTH_SHORT)
                        .show();
                Log.d("sss", listDataChild.get((groupPosition)).get(childPosition).getTitle());
                return false;
            }
        });
        clickButton();

        return view;
    }

 public   List<Services> getServiceList(){
   //  serviceList =  listAdapter.getNumberOfCheckedItemsInGroup();
//     Log.d("ServiceLIst", String.valueOf(listAdapter.getNumberOfCheckedItemsInGroup()));
        return  serviceList;
    }
    private void clickButton() {
        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                serviceList =  listAdapter.getNumberOfCheckedItemsInGroup();
                Log.d("HUY", String.valueOf(serviceList.get(0)));
                bundle.putSerializable("service", (Serializable) serviceList);



                FragmentUtil.replaceNormal(selectMaster, getChildFragmentManager(),  R.id.fragment_mainLayout, bundle);

            }
        });

    }

    private void prepareListData() {

        listDataHeader = new ArrayList<String>();
        listDataHeader.add("Парикмахерские услуги");
        listDataHeader.add("Косметология");
        listDataHeader.add("Ногтевой сервис");
        listDataHeader.add("Услуги массажиста");
    }

    public class LoadServiceAsync extends AsyncTask<String, Void, HashMap<String, List<Services>>> {
        ProgressDialog progressDialog;
        ArrayList<Services> data=null;
        int  d;
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getContext(), "", "Please wait...", true);
        }
        @Override
        protected HashMap<String, List<Services>> doInBackground(String... params) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            haircut = new ArrayList<Services>();
            cosmetology = new ArrayList<Services>();
            manicure = new ArrayList<Services>();
            massage = new ArrayList<Services>();
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder client = new OkHttpClient.Builder();
            client.addInterceptor(loggingInterceptor);
            Retrofit retrofit =
                    new Retrofit.Builder().
                           baseUrl(Constants.BASE_URL).
                          // baseUrl("http://192.168.1.4/").
                           // baseUrl("http://192.168.43.181/").
                            client(client.build()).addConverterFactory(GsonConverterFactory.create()).build();
            RequestInterface request = retrofit.create(RequestInterface.class);
            Call<JSONResponse> call = request.getJSON();
            call.enqueue(new Callback<JSONResponse>() {
                @Override
                public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
                    JSONResponse jsonResponse = response.body();
                    if (jsonResponse != null) {
                        if (data == null) {
                            data = new ArrayList<>(Arrays.asList(jsonResponse.getServices()));
                            for (int i = 0; i < data.size(); i++) {
                                if (data.get(i).getCategory().equals("Парикмахерские услуги")){
                                    haircut.add(data.get(i));
                                } else if (data.get(i).getCategory().equals("Косметология")) {
                                    cosmetology.add(data.get(i));
                                } else if (data.get(i).getCategory().equals("Ногтевой сервис")) {
                                    manicure.add(data.get(i));
                                } else if (data.get(i).getCategory().equals("Услуги массажиста")) {
                                    massage.add(data.get(i));
                                }
                                listDataChild.put(listDataHeader.get(0), haircut); // Header, Child data
                                listDataChild.put(listDataHeader.get(1), cosmetology);
                                listDataChild.put(listDataHeader.get(2), manicure);
                                listDataChild.put(listDataHeader.get(3), massage);
                                Log.d("SUKA", String.valueOf(listDataChild.get(0)));

                            }
                        }
                    }
                }
                @Override
                public void onFailure(Call<JSONResponse> call, Throwable t) {
                    //Заглушка
                    //Log.d("Error", t.getMessage());
                }
            });
            return listDataChild;
        }
        @Override
        protected void onPostExecute( HashMap<String, List<Services>> listDataChild) {
           super.onPostExecute(listDataChild);
          listAdapter = new ExpListViewAdapterWithCheckbox(getContext(), listDataHeader, listDataChild);
           expListView.setAdapter(listAdapter);progressDialog.dismiss();

            }
    }
}