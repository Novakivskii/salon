package com.eetc.solutions.appsalonsugar.loginregistration;


public class Constants {

  //public static final String BASE_URL = "http://192.168.0.145/salon-login-register/";
  public static final String BASE_URL = "http://192.168.0.145/index.php/";
  // public static final String BASE_URL = "http://192.168.1.2/salon-login-register/";
  // public static final String BASE_URL = "http://10.0.3.2/salon-login-register/";
    public static final String REGISTER_OPERATION = "register";
    public static final String LOGIN_OPERATION = "login";
    public static final String CHANGE_PASSWORD_OPERATION = "chgPass";
    public static final String RESET_PASSWORD_INITIATE = "resPassReq";
    public static final String RESET_PASSWORD_FINISH = "resPass";

    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String IS_LOGGED_IN = "isLoggedIn";

    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String UNIQUE_ID = "unique_id";
    public static final String PHONE = "phone";

    public static final String TAG = "salon";

}
