package com.eetc.solutions.appsalonsugar.adapter;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.fragment.OkAppointmentFragment;
import com.eetc.solutions.appsalonsugar.fragment.SelectDateTime;
import com.eetc.solutions.appsalonsugar.fragment.SelectMaster;
import com.eetc.solutions.appsalonsugar.retrofit.Masters;
import com.eetc.solutions.appsalonsugar.retrofit.Services;
import com.eetc.solutions.appsalonsugar.util.FragmentUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MasterAdapter extends RecyclerView.Adapter<MasterAdapter.ViewHolder> {
    private final FragmentManager f_manager;
    private ArrayList<Masters> masters;
    Context context;
    List<Services> mList = new ArrayList<>();
    SelectDateTime selectDateTime = new SelectDateTime();

    public MasterAdapter(ArrayList<Masters> masters, Context context, FragmentManager f_manager,
            List<Services> mList) {
        this.f_manager = f_manager;
        this.masters = masters;
        this.mList = mList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_master, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
        //тут не забыть  добавить
        viewHolder.tv_name.setText(masters.get(i).getName_surname());
        viewHolder.tv_special.setText(String.valueOf(masters.get(i).getDescription()));
        Glide.with(context).load(masters.get(i).getPhoto()).into(viewHolder.imv_master);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundl = new Bundle();
                bundl.putSerializable("service", (Serializable) mList);
                Log.d("MASTER", String.valueOf(mList.get(0)));
                bundl.putString("id_master", String.valueOf(masters.get(i).getId_master()));
                bundl.putString("master", masters.get(i).getName_surname());
                bundl.putString("description", masters.get(i).getDescription());
                FragmentUtil
                        .replaceNormal(selectDateTime, f_manager, R.id.fragment_mainLayout, bundl);

            }
        });

    }

    @Override
    public int getItemCount() {
        return masters.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name, tv_special;
        private ImageView imv_master;

        public ViewHolder(View view) {
            super(view);
            tv_name = (TextView) view.findViewById(R.id.name_master);
            imv_master = (ImageView) view.findViewById(R.id.imv_master);
            tv_special = (TextView) view.findViewById(R.id.special_master);
        }

    }

}
