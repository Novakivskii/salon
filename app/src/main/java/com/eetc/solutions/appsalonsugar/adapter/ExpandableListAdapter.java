package com.eetc.solutions.appsalonsugar.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.retrofit.Services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class ExpandableListAdapter extends BaseExpandableListAdapter {
	private final int[] mGroupDrawables = {
			R.drawable.haircut,
			R.drawable.cosmetology,
			R.drawable.manicure,
			R.drawable.massage,
	};

	private static HashMap<String, Boolean> isSelected;
	private Context _context;
	private List<String> _listDataHeader; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<Services>> _listDataChild;

	public ExpandableListAdapter(Context context, List<String> listDataHeader,
			HashMap<String, List<Services>> listChildData) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;

	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);

	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		Services childText = (Services) getChild(groupPosition, childPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.item_services, null);
		}
		TextView txtServiceTitle = (TextView) convertView.findViewById(R.id.service_name);
		TextView txtPrice = (TextView) convertView.findViewById(R.id.tv_price);
		TextView txtDuration = (TextView) convertView.findViewById(R.id.tvDuration);
		txtServiceTitle.setText(childText.getTitle());
		txtPrice.setText(String.valueOf(childText.getPrice()));
		txtDuration.setText(String.valueOf(childText.getDuration()));

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.service_group, null);
		}
		ImageView image_item= (ImageView)convertView.findViewById(R.id.image_item);
		image_item.setImageResource(mGroupDrawables[groupPosition]);
		TextView lblListHeader = (TextView) convertView.findViewById(R.id.group_name);
		ImageView expandedImage = (ImageView) convertView.findViewById(R.id.item_arrow);
		lblListHeader.setTypeface(null, Typeface.BOLD);
		lblListHeader.setText(headerTitle);

		final int resId = isExpanded ? R.drawable.minus : R.drawable.plus;
		expandedImage.setImageResource(resId);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}


}

