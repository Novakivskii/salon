package com.eetc.solutions.appsalonsugar.retrofit.request;

/**
 * Created by alex on 19.09.2016.
 */
import com.eetc.solutions.appsalonsugar.retrofit.response.RecordResponse;
import com.eetc.solutions.appsalonsugar.retrofit.request.RequestRecordInterface;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RequestRecordInterface {

    @POST("index.php/")
  //  @POST("salon.ru/")
    Call<RecordResponse> operation(@Body ServerRecordRequest request);

}