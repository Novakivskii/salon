package com.eetc.solutions.appsalonsugar.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TableLayout;

import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.activity.HomeActivity;
import com.eetc.solutions.appsalonsugar.fragment.tabslide.RootFragment;
import com.eetc.solutions.appsalonsugar.util.FragmentUtil;

public class SelectServiceMaster extends RootFragment
{
    View view;
    RelativeLayout master, usluga;
    SelectSexFragment select_sex= new SelectSexFragment();
    SelectServiceMaster mSelectServiceMaster;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_select_appointment, container, false);
        master = (RelativeLayout)view.findViewById(R.id.master_layout);
        usluga = (RelativeLayout)view.findViewById(R.id.service_layout);
        mSelectServiceMaster = new SelectServiceMaster();
         clicMaster();
        clicService();
        return view;
    }

    private void clicMaster() {
        master.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentUtil.replaceNormal(select_sex, getChildFragmentManager(),  R.id.fragment_mainLayout, mSelectServiceMaster);
            }
        });
    }

    private void clicService() {
        usluga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentUtil.replaceNormal(select_sex, getChildFragmentManager(),  R.id.fragment_mainLayout, mSelectServiceMaster);
            }
        });

    }

    @Override
    public boolean onBackPressed() {
        return super.onBackPressed();
    }

}
