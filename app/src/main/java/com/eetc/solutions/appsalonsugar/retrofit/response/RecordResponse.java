package com.eetc.solutions.appsalonsugar.retrofit.response;

/**
 * Created by alex on 19.09.2016.
 */
import com.eetc.solutions.appsalonsugar.retrofit.Record;
public class RecordResponse {

    private String result;
    private String message;
    private Record record;

    public String getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }

    public Record getRecord() {
        return record;
    }
}