package com.eetc.solutions.appsalonsugar.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.loginregistration.Constants;
import com.eetc.solutions.appsalonsugar.loginregistration.LoginFragment;
import com.eetc.solutions.appsalonsugar.loginregistration.ProfileFragment;
import com.eetc.solutions.appsalonsugar.loginregistration.RegisterActivity;
import com.eetc.solutions.appsalonsugar.util.FragmentUtil;

/**
 * Created by alex on 19.09.2016.
 */
public class PersonalArea extends Fragment{
 View view;
    private SharedPreferences pref;
    Button bLogin;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_personal_area, container, false);
        bLogin = (Button) view.findViewById(R.id.goLogin);
        pref = getActivity().getPreferences(0);
        final Fragment fragment;
        if(pref.getBoolean(Constants.IS_LOGGED_IN,false)){
            fragment = new FragmentPersonalArea();
        }else {
            fragment = new LoginFragment();
        }
        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                FragmentUtil.replaceNormal(fragment, getChildFragmentManager(),  R.id.fragment_mainLayout);

            }
        });


        return view;
    }


}
