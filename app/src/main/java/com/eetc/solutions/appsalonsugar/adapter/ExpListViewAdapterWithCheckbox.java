package com.eetc.solutions.appsalonsugar.adapter;

/**
 * Created by alex on 15.09.2016.
 */

/**
 * Created by dbhat on 15-03-2016.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.retrofit.Services;

// Eclipse wanted me to use a sparse array instead of my hashmaps, I just suppressed that suggestion
@SuppressLint("UseSparseArrays")
public class ExpListViewAdapterWithCheckbox extends BaseExpandableListAdapter {

    // Define activity context
    private Context mContext;

    /*
     * Here we have a Hashmap containing a String key
     * (can be Integer or other type but I was testing
     * with contacts so I used contact name as the key)
    */
    private HashMap<String, List<Services>> mListDataChild;

    // ArrayList that is what each key in the above
    // hashmap points to
    private ArrayList<String> mListDataGroup;
    private final int[] mGroupDrawables = {
            R.drawable.haircut,
            R.drawable.cosmetology,
            R.drawable.manicure,
            R.drawable.massage,
    };

    // Hashmap for keeping track of our checkbox check states
    private LinkedHashMap<Integer, boolean[]> mChildCheckStates;


    // Our getChildView & getGroupView use the viewholder patter
    // Here are the viewholders defined, the inner classes are
    // at the bottom
    private ChildViewHolder childViewHolder;
    private GroupViewHolder groupViewHolder;
    private String groupText;
    private Services childText;
    private List<Services>  list;
    public ExpListViewAdapterWithCheckbox(Context context, ArrayList<String> listDataGroup,        HashMap<String, List<Services>> listDataChild){
        mContext = context;
        mListDataGroup = listDataGroup;
        mListDataChild = listDataChild;
        mChildCheckStates = new LinkedHashMap<Integer, boolean[]>();

    }

     public List<Services> getNumberOfCheckedItemsInGroup()
    {  list = new ArrayList<>();
        int count = 0;
list = new ArrayList<>();
          for (int j = 0; j < 4; j++) {
            boolean  getChecked[] = mChildCheckStates.get(j);
              if(getChecked != null){
                  for(int i=0; i<getChecked.length; i++){
                if (getChecked[i] == true) {
                    Log.d("j", String.valueOf(j));Log.d("i", String.valueOf(i));
                    count++;
                   if((Services) getChild(j,i)!=null){
                     list.add((Services) getChild(j,i));
                 Log.d("service", String.valueOf(list.get(0)));
                }}
                }}
          }
        return  list;
    }

    public LinkedHashMap<Integer, boolean[]> getValueOfCheckedItemsInGroup()
    {
       return  mChildCheckStates;
    }


    @Override
    public int getGroupCount() {
        return mListDataGroup.size();
     //   Log.isLoggable("dsddd", String.valueOf(mListDataGroup.size()));
    }

    /*
     * This defaults to "public object getGroup" if you auto import the methods
     * I've always make a point to change it from "object" to whatever item
     * I passed through the constructor
    */
    @Override
    public String getGroup(int groupPosition) {
        return mListDataGroup.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {

        //  I passed a text string into an activity holding a getter/setter
        //  which I passed in through "ExpListGroupItems".
        //  Here is where I call the getter to get that text
        groupText = getGroup(groupPosition);

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.service_group, null);

            // Initialize the GroupViewHolder defined at the bottom of this document
            groupViewHolder = new GroupViewHolder();
            groupViewHolder.mGroupText = (TextView) convertView.findViewById(R.id.group_name);
            groupViewHolder.image_item= (ImageView)convertView.findViewById(R.id.image_item);
            groupViewHolder.image_item.setImageResource(mGroupDrawables[groupPosition]);
            groupViewHolder.expandedImage = (ImageView) convertView.findViewById(R.id.item_arrow);
            groupViewHolder. mGroupText.setTypeface(null, Typeface.BOLD);
            final int resId = isExpanded ? R.drawable.minus : R.drawable.plus;
            groupViewHolder.expandedImage.setImageResource(resId);

            convertView.setTag(groupViewHolder);
        } else {

            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }


    groupViewHolder.mGroupText.setText(groupText);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
       return mListDataChild.get(mListDataGroup.get(groupPosition)).size();

    }

    /*
     * This defaults to "public object getChild" if you auto import the methods
     * I've always make a point to change it from "object" to whatever item
     * I passed through the constructor
    */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mListDataChild.get(mListDataGroup.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final int mGroupPosition = groupPosition;
        final int mChildPosition = childPosition;

        //  I passed a text string into an activity holding a getter/setter
        //  which I passed in through "ExpListChildItems".
        //  Here is where I call the getter to get that text
        Services childText = (Services) getChild(groupPosition, childPosition);

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_services, null);

            childViewHolder = new ChildViewHolder();

            childViewHolder.mChildText = (TextView) convertView.findViewById(R.id.service_name);
            childViewHolder.duration = (TextView)convertView.findViewById(R.id.tvDuration);
            childViewHolder.price = (TextView)convertView.findViewById(R.id.tv_price);

            childViewHolder.mCheckBox = (CheckBox) convertView
                    .findViewById(R.id.chkSelected);

            convertView.setTag(R.layout.item_services, childViewHolder);

        } else {

            childViewHolder = (ChildViewHolder) convertView
                    .getTag(R.layout.item_services);
        }

        childViewHolder.mChildText.setText(childText.getTitle());



        childViewHolder.duration.setText(Integer.toString(childText.getDuration()));
        childViewHolder.price.setText(Integer.toString(childText.getPrice()));

		/*
		 * You have to set the onCheckChangedListener to null
		 * before restoring check states because each call to
		 * "setChecked" is accompanied by a call to the
		 * onCheckChangedListener
		*/
        childViewHolder.mCheckBox.setOnCheckedChangeListener(null);

        if (mChildCheckStates.containsKey(mGroupPosition)) {
			/*
			 * if the hashmap mChildCheckStates<Integer, Boolean[]> contains
			 * the value of the parent view (group) of this child (aka, the key),
			 * then retrive the boolean array getChecked[]
			*/
            boolean getChecked[] = mChildCheckStates.get(mGroupPosition);

            // set the check state of this position's checkbox based on the
            // boolean value of getChecked[position]
            childViewHolder.mCheckBox.setChecked(getChecked[mChildPosition]);


        } else {

			/*
			 * if the hashmap mChildCheckStates<Integer, Boolean[]> does not
			 * contain the value of the parent view (group) of this child (aka, the key),
			 * (aka, the key), then initialize getChecked[] as a new boolean array
			 *  and set it's size to the total number of children associated with
			 *  the parent group
			*/
            boolean getChecked[] = new boolean[getChildrenCount(mGroupPosition)];

            // add getChecked[] to the mChildCheckStates hashmap using mGroupPosition as the key
            mChildCheckStates.put(mGroupPosition, getChecked);

            // set the check state of this position's checkbox based on the
            // boolean value of getChecked[position]
            childViewHolder.mCheckBox.setChecked(false);
        }

        childViewHolder.mCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    boolean getChecked[] = mChildCheckStates.get(mGroupPosition);
                    getChecked[mChildPosition] = isChecked;
                    mChildCheckStates.put(mGroupPosition, getChecked);

                } else {

                    boolean getChecked[] = mChildCheckStates.get(mGroupPosition);
                    getChecked[mChildPosition] = isChecked;
                    mChildCheckStates.put(mGroupPosition, getChecked);
                }
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    public final class GroupViewHolder {

        ImageView  image_item;
        ImageView expandedImage;
        TextView mGroupText;
    }

    public final class ChildViewHolder {

        TextView mChildText;
        TextView  price;
        TextView duration;
        CheckBox mCheckBox;
    }
}

