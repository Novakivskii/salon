package com.eetc.solutions.appsalonsugar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.retrofit.Services;

import java.util.List;

/**
 * Created by alex on 18.09.2016.
 */
 public class ListAppointAdapter extends ArrayAdapter<Services> {
TextView txt;
    Context context;
    int layoutResourceId;
    List<Services> service = null;

    public ListAppointAdapter(Context context, int  layoutResourceId, List<Services> service)  {
        super(context, layoutResourceId, service);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.service= service;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Services services = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(layoutResourceId, null);
        }

       txt =  (TextView) convertView.findViewById(R.id.service_item);
        txt.setText(services.getTitle());

        return convertView;
    }
}