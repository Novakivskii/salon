package com.eetc.solutions.appsalonsugar.fragment.tabslide;



import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.adapter.ViewPagerAdapter;
import com.viewpagerindicator.TabPageIndicator;
import  com.viewpagerindicator.IconPageIndicator;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class CarouselFragment extends Fragment  {

    /**
     * TabPagerIndicator
     *
     * Please refer to ViewPagerIndicator library
     */
    protected TabLayout indicator;



    protected ViewPager pager;

    private ViewPagerAdapter adapter;



    public CarouselFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_carousel, container, false);

        indicator = (TabLayout) rootView.findViewById(R.id.tab_layout);

        pager = (ViewPager) rootView.findViewById(R.id.pager);


        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Note that we are passing childFragmentManager, not FragmentManager
        adapter = new ViewPagerAdapter(getResources(), getChildFragmentManager());
        pager.setAdapter(adapter);
        indicator.setupWithViewPager(pager);
        indicator.getTabAt(0).setIcon(R.drawable.ic_home).setText(R.string.salon);
        indicator.getTabAt(1).setIcon(R.drawable.ic_info).setText(R.string.personal_area);
        indicator.getTabAt(2).setIcon(R.drawable.ic_border).setText(R.string.appointment);
        indicator.getTabAt(3).setIcon(R.drawable.ic_star).setText(R.string.news_and_event);

        // indicator.setViewPager(pager);



    }

    /**
     * Retrieve the currently visible Tab Fragment and propagate the onBackPressed callback
     *
     * @return true = if this fragment and/or one of its associates Fragment can handle the backPress
     */
    public boolean onBackPressed() {
        // currently visible tab Fragment
        OnBackPressListener currentFragment = (OnBackPressListener) adapter.getRegisteredFragment(pager.getCurrentItem());

        if (currentFragment != null) {
            // lets see if the currentFragment or any of its childFragment can handle onBackPressed
            return currentFragment.onBackPressed();
        }

        // this Fragment couldn't handle the onBackPressed call
        return false;
    }



}
