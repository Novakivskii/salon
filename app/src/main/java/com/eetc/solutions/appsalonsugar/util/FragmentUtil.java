package com.eetc.solutions.appsalonsugar.util;

        /**
         * Created by alex on 17.08.2016.
         */
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.support.v4.app.FragmentManager;
        import android.support.v4.app.FragmentTransaction;

public class FragmentUtil {

    public static void replaceNormal(Fragment fragment, FragmentManager fragmentManager, int id) {
        fragmentManager.beginTransaction().replace(id, fragment).addToBackStack(null).commit();
    }

    public static void replaceNormal(Fragment fragment, FragmentManager fragmentManager, int id, Bundle args) {
        fragment.setArguments(args);
        fragmentManager.beginTransaction().replace(id, fragment).commit();
    }

    public static void replaceNormal(Fragment fragment, FragmentManager fragmentManager, int id, Fragment currentFragment) {
        fragmentManager.beginTransaction().replace(id, fragment).hide(currentFragment).commit();
    }
    public static void replaceNormal(Fragment fragment, FragmentManager fragmentManager, int id, Fragment currentFragment, Bundle args) {
        fragment.setArguments(args);
        fragmentManager.beginTransaction().replace(id, fragment).hide(currentFragment).commit();
    }

    public static void replaceAddToBack(Fragment fragment, FragmentManager fragmentManager, int id) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(id, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
 
    }

    public static void replaceAddToBack(Fragment fragment, FragmentManager fragmentManager, int id, Bundle args) {
        fragment.setArguments(args);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(id, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}