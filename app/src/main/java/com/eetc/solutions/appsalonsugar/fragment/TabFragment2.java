package com.eetc.solutions.appsalonsugar.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.retrofit.Services;
import com.eetc.solutions.appsalonsugar.adapter.ServiceAdapter;
import com.eetc.solutions.appsalonsugar.retrofit.response.JSONResponse;
import com.eetc.solutions.appsalonsugar.retrofit.request.RequestInterface;
import com.eetc.solutions.appsalonsugar.util.FragmentUtil;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TabFragment2 extends Fragment {
    View view;
    private RecyclerView mRecyclerView;
    private ArrayList<Services>  data;
    SelectDateTime  test= new SelectDateTime();
    private ServiceAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tab_fragment_2, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        loadJSON();

        //initViews();
        return view;
    }
/*
    private void initViews() {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        loadJSON();
    }*/

    private void loadJSON() {


        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder();

        client.addInterceptor(loggingInterceptor);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.145")
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<JSONResponse> call = request.getJSON();
        call.enqueue(new Callback<JSONResponse>() {

            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
                JSONResponse jsonResponse = response.body();
                if(jsonResponse!=null) {
                    if(data==null){
                        data = new ArrayList<>(Arrays.asList(jsonResponse.getServices()));}
                    mAdapter = new ServiceAdapter(data, getContext(),  getFragmentManager());
                    mRecyclerView.setAdapter(mAdapter);
                    Log.d("get",  data.get(0).getImage());

                }
                //
            }


            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                //Заглушка
                //Log.d("Error", t.getMessage());
            }

        });
    }
}