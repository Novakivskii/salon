package com.eetc.solutions.appsalonsugar.loginregistration;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import com.eetc.solutions.appsalonsugar.loginregistration.models.ServerRequest;
import com.eetc.solutions.appsalonsugar.loginregistration.models.ServerResponse;

public interface RequestInterface {

    @POST("salon.ru/")
    Call<ServerResponse> operation(@Body ServerRequest request);

}
