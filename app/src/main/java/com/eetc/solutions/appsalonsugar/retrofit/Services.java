package com.eetc.solutions.appsalonsugar.retrofit;

import java.io.Serializable;

public class Services implements Serializable{

    private int id_service;
    private String title;
    private String description;
    private int price;
    private int duration;
    private String sex;
    private boolean selected  = false;

    public boolean isSelected() {
        return selected;


    }
    public void setSelected(boolean selected){
        this.selected = selected;
    }

    private void setId_service(int id_service) {
        this.id_service = id_service;
    }

    private void setTitle(String title) {
        this.title = title;
    }

    private void setPrice(int price) {
        this.price = price;
    }

    private void setCategory(String category) {
        this.category = category;
    }

    private void setSex(String sex) {
        this.sex = sex;
    }

    private void setDuration(int duration) {
        this.duration = duration;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    private void setImage(String image) {
        this.image = image;
    }

    private String category;
    private String image;

    @Override
    public String toString() {
        return "Services{" +
                "id_service=" + id_service +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", duration=" + duration +
                ", sex='" + sex + '\'' +
                ", category='" + category + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    private Services(int id_service, String title, String description, int price, int duration,
            String sex, String category, String image) {
        this.id_service = id_service;
        this.title = title;
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.sex = sex;
        this.category = category;
        this.image = image;
    }



    public int getId_service() {
        return id_service;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public int getDuration() {
        return duration;
    }

    public String getSex() {
        return sex;
    }

    public String getCategory() {
        return category;
    }

    public String getImage() {
        return image;
    }


}