package com.eetc.solutions.appsalonsugar.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.adapter.MasterAdapter;
import com.eetc.solutions.appsalonsugar.retrofit.Services;
import com.eetc.solutions.appsalonsugar.retrofit.response.MasterJsonResponse;
import com.eetc.solutions.appsalonsugar.retrofit.request.RequestMasterInterface;
import com.eetc.solutions.appsalonsugar.retrofit.Masters;
import com.eetc.solutions.appsalonsugar.util.FragmentUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alex on 07.09.2016.
 */
public class SelectMaster  extends Fragment {
    View view;
    private RecyclerView mRecyclerView;
    private ArrayList<Masters> master;
    private MasterAdapter mAdapter;
    SelectMaster selectMaster;
    List<Services> list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.select_master, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_master);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        loadJSON();
        return view;
    }
/*
 private void defaut_click() {
     defaut_master.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             bundl.putSerializable("service", (Serializable) list);
             bundl.putString("master",  "любой мастер");
             FragmentUtil.replaceNormal(new SelectDateTime(),  getChildFragmentManager(),  R.id.fragment_mainLayout, bundl);
         }
     });

    }
*/
/*
    private void initViews() {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        loadJSON();
    }*/

    private void loadJSON() {


        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder();

        client.addInterceptor(loggingInterceptor);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.145")
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestMasterInterface request = retrofit.create(RequestMasterInterface.class);
        Call<MasterJsonResponse> call = request.getJSON();
        call.enqueue(new Callback<MasterJsonResponse>() {

            @Override
            public void onResponse(Call<MasterJsonResponse> call, Response<MasterJsonResponse> response) {
                MasterJsonResponse jsonResponse = response.body();

                if(jsonResponse!=null) {
                     list  = new ArrayList<Services>();
                     list = (List<Services>) getArguments().getSerializable("service");
                    Log.d("HUY", String.valueOf(list.get(0)));
                 //   getArguments().remove("service");
//                    Log.d("ALIST", String.valueOf(list.get(0)));
                    if(master==null){master = new ArrayList<>(Arrays.asList(jsonResponse.getMasters()));}
                    mAdapter = new MasterAdapter(master, getActivity(),  getChildFragmentManager(), list);
                    mRecyclerView.setAdapter(mAdapter);


                }

                //
            }


            @Override
            public void onFailure(Call<MasterJsonResponse> call, Throwable t) {
                //Заглушка
                //Log.d("Error", t.getMessage());
            }

        });
    }
}
