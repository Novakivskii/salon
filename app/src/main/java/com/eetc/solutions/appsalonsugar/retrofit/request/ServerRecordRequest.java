package com.eetc.solutions.appsalonsugar.retrofit.request;

import com.eetc.solutions.appsalonsugar.retrofit.Record;

/**
 * Created by alex on 19.09.2016.
 */
public class ServerRecordRequest {

    private String operation;
    private Record record;

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setRecord(Record record) {
        this.record = record;
    }
}