package com.eetc.solutions.appsalonsugar.adapter;

import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.eetc.solutions.appsalonsugar.fragment.PersonalArea;
import com.eetc.solutions.appsalonsugar.fragment.SelectServiceMaster;
import com.eetc.solutions.appsalonsugar.fragment.TabFragment1;
import com.eetc.solutions.appsalonsugar.fragment.TabFragment2;

/**
 * Created by shahabuddin on 6/6/14.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    private final Resources resources;

    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    public ViewPagerAdapter(final Resources resources, FragmentManager fm) {
        super(fm);
        this.resources = resources;
    }

    @Override
    public Fragment getItem(int position) {
        final Fragment result;
        switch (position) {
            case 0:
                // First Fragment of First Tab
                result = new TabFragment1();
                break;
            case 1:
                // First Fragment of Second Tab
                result = new PersonalArea();
                break;
            case 2:
                // First Fragment of Third Tab
                result = new SelectServiceMaster();
                break;
            case 3:
                // First Fragment of Third Tab
                result = new TabFragment2();
                break;
            default:
                result = null;
                break;
        }

        return result;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }


}
