package com.eetc.solutions.appsalonsugar.retrofit;

/**
 * Created by alex on 07.09.2016.
 */
public class Masters {
    private  int id_master;
    private String name_surname;
    private String description;
    private  int time_work;
    private String photo;

    public String getName_surname() {
        return name_surname;
    }

    public void setName_surname(String name_surname) {
        this.name_surname = name_surname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public  int getId_master() {
        return id_master;
    }

    public void setId_master(int id_master) {
        this.id_master = id_master;
    }

    public int getTime_work() {
        return time_work;
    }

    public void setTime_work(int time_work) {
        this.time_work = time_work;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
