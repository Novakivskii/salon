package com.eetc.solutions.appsalonsugar.adapter;


import android.content.Context;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.fragment.SelectDateTime;
import com.eetc.solutions.appsalonsugar.fragment.SelectServices;
import com.eetc.solutions.appsalonsugar.fragment.SelectSexFragment;
import com.eetc.solutions.appsalonsugar.retrofit.Services;
import com.eetc.solutions.appsalonsugar.util.FragmentUtil;

import java.util.ArrayList;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {

    private ArrayList<Services> services;
    FragmentManager fm;
    SelectDateTime test = new SelectDateTime();
    Context  context;

    public ServiceAdapter(ArrayList<Services> services,  Context context,  FragmentManager  fm) {
        this.services = services;
        this.context=context;
        this.fm=fm;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_service, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
//тут не забыть  добавить
        viewHolder.tv_title.setText(services.get(i).getTitle());
        viewHolder.tv_price.setText(String.valueOf(services.get(i).getPrice()));
        viewHolder.tv_duration.setText(String.valueOf(services.get(i).getDuration()));
        Glide.with(context)
                .load(services.get(i).getImage())
                .into(viewHolder.imv_service);
      //         viewHolder.imv_service.setImageURI(Uri.parse(services.get(i).getImage()));;
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentUtil.replaceNormal(new SelectSexFragment(), fm, R.id.fragment_mainLayout);
            }
        });

    }
    @Override
    public int getItemCount() {
        return  services.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_title,tv_price, tv_duration;
        private ImageView  imv_service;
        public ViewHolder(View view) {
            super(view);
            tv_title= (TextView)view.findViewById(R.id.tv_title);
            imv_service = (ImageView)view.findViewById(R.id.imv_service);
            tv_price = (TextView)view.findViewById(R.id.tv_price);
            tv_duration = (TextView)view.findViewById(R.id.tv_duration);


        }
    }

}