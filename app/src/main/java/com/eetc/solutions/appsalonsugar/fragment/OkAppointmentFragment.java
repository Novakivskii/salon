package com.eetc.solutions.appsalonsugar.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.adapter.ExpListViewAdapterWithCheckbox;
import com.eetc.solutions.appsalonsugar.adapter.ListAppointAdapter;
import com.eetc.solutions.appsalonsugar.adapter.MasterAdapter;
import com.eetc.solutions.appsalonsugar.loginregistration.models.ServerResponse;
import com.eetc.solutions.appsalonsugar.retrofit.Constants;
import com.eetc.solutions.appsalonsugar.retrofit.Record;
import com.eetc.solutions.appsalonsugar.retrofit.Services;
import com.eetc.solutions.appsalonsugar.retrofit.request.RequestRecordInterface;
import com.eetc.solutions.appsalonsugar.retrofit.request.ServerRecordRequest;
import com.eetc.solutions.appsalonsugar.retrofit.response.RecordResponse;


import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alex on 11.09.2016.
 */
public class OkAppointmentFragment  extends Fragment {
    View  view;
    TextView time_date, master, price, time;
    ListView  service;
    Integer id_master, id_service;
    EditText name, surname, phone;
    private int sum_price, sum_time;
    ListAppointAdapter adapter;
    List<Services> serviceList;
    String sDate, sTime;
    Button  recordButton;
    Bundle bundle;
    private SharedPreferences pref;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ok_appointment, container, false);
        recordButton = (Button) view.findViewById(R.id.record_button);
        bundle = getArguments();
        pref = getActivity().getPreferences(0);
        if(new Bundle().getString("master")!=null){
        Log.d("Date", bundle.getString("date"));
        Log.d("Date", bundle.getString("master"));}
        init();
        record();
        return  view;
    }

    private void init() {

        serviceList = new ArrayList<>();
        serviceList = (List<Services>) getArguments().getSerializable("service");
        if (serviceList == null){Log.d("LIST", "NULLL");}
        time_date = (TextView) view.findViewById(R.id.time_date);
        service = (ListView) view.findViewById(R.id.service);
        master = (TextView) view.findViewById(R.id.master);
        price = (TextView) view.findViewById(R.id.price);
        time = (TextView) view.findViewById(R.id.time);
        name = (EditText) view.findViewById(R.id.your_name);
        phone = (EditText) view.findViewById(R.id.your_phone);
        sDate = bundle.getString("date");
        sTime = bundle.getString("time");
        time_date.setText(sDate +" в "+ sTime);
        master.setText(bundle.getString("master"));

        String sname  = pref.getString(com.eetc.solutions.appsalonsugar.loginregistration.Constants.NAME,"");
      name.setText(sname);
        String sphone  = pref.getString(com.eetc.solutions.appsalonsugar.loginregistration.Constants.PHONE,"");
        phone.setText(sphone);



        //        Log.d("ID_MASTER", bundle.getString("id _master"));
                id_master= 3;
//        id_master = Integer.valueOf(bundle.getString("id _master"));
//        id_service = serviceList.get(0).getId_service();
        ArrayAdapter<Services> adapter = new ListAppointAdapter(getContext(),R.layout.item_list_appointment, serviceList);
        service.setAdapter(adapter);
        for(int i=0; i<serviceList.size(); i++){
            sum_price+= serviceList.get(i).getPrice();
            sum_time+= serviceList.get(i).getDuration();
        }
        time.setText( Integer.toString(sum_time));
        price.setText(Integer.toString(sum_price));


    }
    private void record() {
        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

         /*    if(!id_service.isEmpty() && !email.isEmpty() && !password.isEmpty()) {

                    progress.setVisibility(View.VISIBLE);
                    registerProcess(name,email,password);
                }
                else {

                    Snackbar.make(getView(), "Ошибка  в  данных!", Snackbar.LENGTH_LONG).show();
                }
                */


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://192.168.0.145/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                RequestRecordInterface requestInterface = retrofit.create(RequestRecordInterface.class);

                Record record = new Record();
                record.setId_service(id_service);
                record.setId_master(id_service);
                record.setDate(sDate);
                record.setTime(sTime);

                ServerRecordRequest request = new ServerRecordRequest();
                request.setOperation("record");
                request.setRecord(record);
                Call<RecordResponse> response = requestInterface.operation(request);

                response.enqueue(new Callback<RecordResponse>() {
                    @Override
                    public void onResponse(Call<RecordResponse> call, retrofit2.Response<RecordResponse> response) {

                        RecordResponse resp = response.body();
//                        Snackbar.make(getView(), resp.getMessage(), Snackbar.LENGTH_LONG).show();
                        //progress.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onFailure(Call<RecordResponse> call, Throwable t) {

                     //   progress.setVisibility(View.INVISIBLE);
                        Log.d("EROR RECORD","failed");
             //           Snackbar.make(getView(), t.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();

                    }
                });
            }


        });
    }


}
