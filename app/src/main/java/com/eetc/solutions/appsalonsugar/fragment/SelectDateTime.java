package com.eetc.solutions.appsalonsugar.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.view.ViewGroup.LayoutParams;


import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.retrofit.Services;
import com.eetc.solutions.appsalonsugar.util.FragmentUtil;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import noman.weekcalendar.WeekCalendar;
import noman.weekcalendar.listener.OnDateClickListener;

/**
 * Created by alex on 06.09.2016.
 */
public class SelectDateTime extends Fragment{
    View   view;
    TextView textView;
    private WeekCalendar weekCalendar;
    TextView date;
    String mDate;
    SimpleDateFormat dateFormat = null;
    private String[] time = {
            "10:00",  "10:15",  "10:30", "10.45",
            "11:00",  "11:15",  "11:30", "11.45",
            "12:00",  "12:15",  "12:30", "12.45",
            "13:00",  "13:15",  "13:30", "13.45",
            "14:00",  "14:15",  "14:30", "14.45",
            "15:00",  "15:15",  "15:30", "15.45",
            "16:00",  "16:15",  "16:30", "16.45",
            "10:00",  "10:15",  "17:30", "17.45",
            "18:00",  "18:15",  "18:30", "18.45",

    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.select_date_time, container, false);
        textView = (TextView) view.findViewById(R.id.tTime);
        date = (TextView) view.findViewById(R.id.selectDate);
        dateFormat = new SimpleDateFormat("dd MMMM yyyy", new Locale("ru","RU"));
        init();
        TableTime();

        return  view;

    }
    private void init() {
        weekCalendar = (WeekCalendar) view.findViewById(R.id.weekCalendar);

        weekCalendar.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(DateTime dateTime) {
                Toast.makeText(getActivity(), "Вы  выбрали  " +dateTime.toString(), Toast
                        .LENGTH_SHORT).show();
                date.setText("Вы  выбрали: " +  dateFormat.format( dateTime.toDate()));
             //   mDate=dateTime.toString();
               mDate=dateFormat.format(dateTime.toDate());

            }

        });

      date.setText("Выбранная   дата:  " +  dateFormat.format(new DateTime().toLocalDate().toDate()));
        //mDate=new DateTime().toLocalDate().toString();
        mDate=dateFormat.format(new DateTime().toLocalDate().toDate());
        /*
        weekCalendar.setOnWeekChangeListener(new OnWeekChangeListener() {
            @Override
            public void onWeekChange(DateTime firstDayOfTheWeek, boolean forward) {
                Toast.makeText(getActivity(), "Week changed: " + firstDayOfTheWeek +
                        " Forward: " + forward, Toast.LENGTH_SHORT).show();
            }
        });*/
    }


    public void onNextClick(View veiw) {
        weekCalendar.moveToNext();
    }


    public void onPreviousClick(View view) {
        weekCalendar.moveToPrevious();
    }

    public void onResetClick(View view) {
        weekCalendar.reset();

    }
    public void onSelectedDateClick(View view){
        weekCalendar.setSelectedDate(new DateTime().plusDays(50));
    }
    public void onStartDateClick(View view){
        weekCalendar.setStartDate(new DateTime().plusDays(7));
    }
    private void TableTime() {
       // this.time = time;
        int table_row;
        TableLayout tableLayout = null;
        TableRow tableRow;
        tableLayout = (TableLayout) view.findViewById(R.id.table_time);
        tableLayout.removeAllViews();
        if (time.length < 36) {
            table_row = 8;
        } else if (time.length >35) {
            table_row = 9;
        } else table_row= 10;

        int z = 0;
        int table_col = 4;
        TableLayout.LayoutParams tableLayoutParams = new TableLayout.LayoutParams();
        if (time.length > 0) {
            for (int i = 0; i < table_row; i++) {
                tableRow = new TableRow(getContext());
                TableRow.LayoutParams tableRowParams = new TableRow.LayoutParams();
                 tableRowParams.setMargins(10,0,0,10);
                for (int j = 0; j < table_col; j++) {
                  final TextView textView = new TextView(getContext());
                  textView.setTextSize(getResources().getDimension(R.dimen.time));
                    textView.setWidth((int) getResources().getDimension(R.dimen.WbtnTime));
                    textView.setWidth((int) getResources().getDimension(R.dimen.HbtnTime));
                    textView.setGravity(Gravity.CENTER_HORIZONTAL);
                    textView.setTextColor(getResources().getColor(R.color.colorYellow));
                    textView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    textView.setLayoutParams(tableRowParams);
                    if (z >= time.length) {
                      textView.setText(" ");
                    } else {
                      //  LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                      //  textView = new TextView(getContext());
                   textView.setText(time[z]);
                    }
                    textView.setOnClickListener(new TextView.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            List<Services> list = new ArrayList<>();
                            list = (List<Services>) getArguments().getSerializable("service");
                            getArguments().remove("service");
                            Bundle bundl = new Bundle();
                            bundl.putString("master", getArguments().getString("master"));
                            bundl.putString("id_master", getArguments().getString("id_master"));
                            bundl.putString("date",mDate);
                            bundl.putSerializable("service", (Serializable) list);
                            Log.d("MASTER", String.valueOf(list.get(0)));
                            bundl.putString("time", String.valueOf(textView.getText()));
                            FragmentUtil.replaceNormal(new OkAppointmentFragment(),getChildFragmentManager(),  R.id.fragment_mainLayout, bundl);
                        }
                    });
                    tableRow.addView(textView, j);
                    z++;
                }
                tableLayout.addView(tableRow, i);
            }

        }
    }
}