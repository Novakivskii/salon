package com.eetc.solutions.appsalonsugar.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.loginregistration.Constants;
import com.eetc.solutions.appsalonsugar.loginregistration.LoginFragment;
import com.eetc.solutions.appsalonsugar.util.FragmentUtil;

/**
 * Created by alex on 19.09.2016.
 */
public class FragmentPersonalArea extends Fragment {
    View view;
    private SharedPreferences pref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_personal_area, container, false);

        return view;
    }

}