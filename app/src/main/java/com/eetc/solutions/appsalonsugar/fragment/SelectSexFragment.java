package com.eetc.solutions.appsalonsugar.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.eetc.solutions.appsalonsugar.R;
import com.eetc.solutions.appsalonsugar.activity.HomeActivity;
import com.eetc.solutions.appsalonsugar.fragment.tabslide.RootFragment;
import com.eetc.solutions.appsalonsugar.util.FragmentUtil;

public class SelectSexFragment extends RootFragment{
    View view;
    RelativeLayout man, woman;
    SelectServices mSelectServices;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_select_sex, container, false);

        man = (RelativeLayout) view.findViewById(R.id.man_layout);
        woman = (RelativeLayout) view.findViewById(R.id.woman_layout);
        mSelectServices = new SelectServices();
        clickMan();
        clickWoman();
        return view;}

    private void clickMan() {
        man.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            FragmentUtil.replaceNormal(mSelectServices, getChildFragmentManager(),  R.id.fragment_mainLayout);
        }
    });

    }

    private void clickWoman() {
        woman.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            FragmentUtil.replaceNormal(mSelectServices, getChildFragmentManager(),  R.id.fragment_mainLayout);

        }
    });

    }

    @Override
    public boolean onBackPressed() {
        return super.onBackPressed();
    }
}







