package com.eetc.solutions.appsalonsugar.retrofit;

/**
 * Created by alex on 19.09.2016.
 */
public class Constants {

    public static final String BASE_URL = "http://192.168.0.145/";
    public static final String RECORD_OPERATION = "record";
    public static final String LOGIN_OPERATION = "login";
    public static final String CHANGE_PASSWORD_OPERATION = "chgPass";

    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String IS_LOGGED_IN = "isLoggedIn";

    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String UNIQUE_ID = "unique_id";

}