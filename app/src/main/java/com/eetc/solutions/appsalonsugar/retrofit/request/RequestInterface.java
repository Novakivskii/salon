package com.eetc.solutions.appsalonsugar.retrofit.request;

import com.eetc.solutions.appsalonsugar.retrofit.response.JSONResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RequestInterface {
    @GET("index.php?returnListServ")
    Call<JSONResponse> getJSON();
}
