package com.eetc.solutions.appsalonsugar.retrofit.request;

import com.eetc.solutions.appsalonsugar.retrofit.response.JSONResponse;
import com.eetc.solutions.appsalonsugar.retrofit.response.MasterJsonResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RequestMasterInterface {
    @GET("index.php?AllMaster")
    Call<MasterJsonResponse> getJSON();
}
